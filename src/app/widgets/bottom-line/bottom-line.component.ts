import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-bottom-line',
  templateUrl: './bottom-line.component.html',
  styleUrls: ['./bottom-line.component.scss'],
})
export class BottomLineComponent implements OnInit {
  @Input() show = false;
  @Input() width = '40%';
  @Input() marginTop = '0.6rem';
  constructor() {}

  ngOnInit(): void {}

  get top() {
    return `calc(100% + ${this.marginTop})`;
  }
}
