import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BottomLineComponent } from './bottom-line.component';

@NgModule({
  declarations: [BottomLineComponent],
  imports: [CommonModule],
  exports: [BottomLineComponent],
})
export class BottomLineModule {}
