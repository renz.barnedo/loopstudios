import { Component, Input, OnInit } from '@angular/core';

export type Icon = 'facebook' | 'twitter' | 'pinterest' | 'instagram';

@Component({
  selector: 'app-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.scss'],
})
export class IconComponent implements OnInit {
  @Input() icon: Icon = 'facebook';

  constructor() {}

  ngOnInit(): void {}
}
