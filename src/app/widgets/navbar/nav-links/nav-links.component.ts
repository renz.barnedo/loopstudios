import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav-links',
  templateUrl: './nav-links.component.html',
  styleUrls: ['./nav-links.component.scss'],
})
export class NavLinksComponent implements OnInit {
  @Input() color = 'light';
  @Input() gap = '2.4rem';
  links = [
    {
      label: 'About',
      link: '#',
    },
    {
      label: 'Careers',
      link: '#',
    },
    {
      label: 'Events',
      link: '#',
    },
    {
      label: 'Products',
      link: '#',
    },
    {
      label: 'Support',
      link: '#',
    },
  ];
  shownBottomLine = -1;

  constructor() {}

  ngOnInit(): void {}
}
