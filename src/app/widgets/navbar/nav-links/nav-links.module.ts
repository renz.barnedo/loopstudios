import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavLinksComponent } from './nav-links.component';
import { BottomLineModule } from '../../bottom-line/bottom-line.module';

@NgModule({
  declarations: [NavLinksComponent],
  imports: [CommonModule, BottomLineModule],
  exports: [NavLinksComponent],
})
export class NavLinksModule {}
