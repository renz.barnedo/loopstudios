import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar.component';
import { NavLinksModule } from './nav-links/nav-links.module';
import { LogoModule } from '../logo/logo.module';

@NgModule({
  declarations: [NavbarComponent],
  imports: [CommonModule, NavLinksModule, LogoModule],
  exports: [NavbarComponent],
})
export class NavbarModule {}
