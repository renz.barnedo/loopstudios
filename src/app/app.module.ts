import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { CreationsModule } from './sections/creations/creations.module';
import { FeatureModule } from './sections/feature/feature.module';
import { FooterModule } from './sections/footer/footer.module';
import { HeroModule } from './sections/hero/hero.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    HeroModule,
    FeatureModule,
    CreationsModule,
    FooterModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
