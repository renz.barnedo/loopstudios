import { Component, OnInit } from '@angular/core';
import { Icon } from 'src/app/widgets/icon/icon.component';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  icons: Icon[] = ['facebook', 'twitter', 'pinterest', 'instagram'];
  width = window.innerWidth;
  currentYear = new Date().getFullYear();
  shownBottomLine = -1;

  constructor() {}

  ngOnInit(): void {}
}
