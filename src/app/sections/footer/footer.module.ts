import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer.component';
import { LogoModule } from 'src/app/widgets/logo/logo.module';
import { NavLinksModule } from 'src/app/widgets/navbar/nav-links/nav-links.module';
import { IconModule } from 'src/app/widgets/icon/icon.module';
import { BottomLineModule } from 'src/app/widgets/bottom-line/bottom-line.module';

@NgModule({
  declarations: [FooterComponent],
  imports: [
    CommonModule,
    LogoModule,
    NavLinksModule,
    IconModule,
    BottomLineModule,
  ],
  exports: [FooterComponent],
})
export class FooterModule {}
