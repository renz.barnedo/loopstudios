import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreationsComponent } from './creations.component';

@NgModule({
  declarations: [CreationsComponent],
  imports: [CommonModule],
  exports: [CreationsComponent],
})
export class CreationsModule {}
