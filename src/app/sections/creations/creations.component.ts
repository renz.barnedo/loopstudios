import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-creations',
  templateUrl: './creations.component.html',
  styleUrls: ['./creations.component.scss'],
})
export class CreationsComponent implements OnInit {
  images = [
    {
      name: 'image-deep-earth.jpg',
      label: 'deep earth',
    },
    {
      name: 'image-night-arcade.jpg',
      label: 'night arcade',
    },
    {
      name: 'image-soccer-team.jpg',
      label: 'soccer team vr',
    },
    {
      name: 'image-grid.jpg',
      label: 'the grid',
    },
    {
      name: 'image-from-above.jpg',
      label: 'from up above vr',
    },
    {
      name: 'image-pocket-borealis.jpg',
      label: 'pocket borealis',
    },
    {
      name: 'image-curiosity.jpg',
      label: 'the curiosity',
    },
    {
      name: 'image-fisheye.jpg',
      label: 'make it fisheye',
    },
  ];

  constructor() {}

  ngOnInit(): void {}

  getImagePath(imageName: string) {
    const type = window.innerWidth > 700 ? 'desktop' : 'mobile';
    return `../../../assets/images/${type}/${imageName}`;
  }
}
